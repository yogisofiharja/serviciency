from fabric.api import env,run, local
from pymongo import *
import datetime
client = MongoClient('localhost', 27017)
db = client.serviciency
node = db.nodes
list_node = []
for node in node.find():
	list_node.append(node["ip_address"])



def singleHost(ip_address):
	env.hosts=[ip_address]
	env.user="node"
	env.password="razorblade"

def allHost():
	env.hosts=list_node
	env.user="node"
	env.password="razorblade"	

def getUptime():
	uptime = run("uptime")
	print uptime
	print env.host_string
	uptime = uptime.split(' ')
	output = []
	load = ""
	minutes = ""
	days = ""
	for element in uptime:
		if element != '':
			output.append(element.strip(', :'))

	if output[3] == "min":
		days = 0
		minutes = (int(days)*1440)+int(output[2])
		load = output[9]
	elif output[3] == "day" or output[3]=="days":
		if output[5] !="min":
			output[4]=output[4].split(':')
			output[4] = (int(output[4][0])*60)+int(output[4][1])

		days = output[2]
		minutes = (int(days)*1440)+int(output[4])
		load = output[10]
	elif output[4] == "user" or output[4]=="users":
		output[2]=output[2].split(':')
		output[2] = (int(output[2][0])*60)+int(output[2][1])
		days = 0
		minutes = (int(days)*1440)+int(output[2])
		load = output[8]

	date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
	logs = db.logs
	log = {"ip_address":env.host_string,"uptime":minutes, "load":load,"date":date}
	print log
	logs.insert(log)

def shutitdown():
	run("sudo shutdown -h now")
