from pymongo import *
from datetime import datetime
import re
import argparse
import numpy as np

def find_min_load_time(dicto):
    temp_v = 999999999999999
    temp_k = {}

    for k in dicto:
        if k['load'] < temp_v :
            temp_v = k['load']
            temp_k = k

    return temp_k

def find_max_load_time(dicto):
    temp_v = 0.0
    temp_k = {}

    for k in dicto:
        if k['load'] > temp_v :
            temp_v = k['load']
            temp_k = k

    return temp_k 

def find_min_up_time(dicto):
    temp_v = 999999999999999
    temp_k = {}

    for k in dicto:
        if k['uptime'] < temp_v :
            temp_v = k['uptime']
            temp_k = k

    return temp_k

def find_max_up_time(dicto):
    temp_v = 0
    temp_k = {}

    for k in dicto:
        if k['uptime'] > temp_v :
            temp_v = k['uptime']
            temp_k = k

    return temp_k

def is_uptime_similar(dicto):
    is_similar = False
    temp_uptime = dicto[0]['uptime']
    for k in dicto[1:len(dicto)]:
        if k['uptime'] == temp_uptime:
            is_similar = True
        else:
            is_similar = False

    return is_similar

# menggunakan argument parser untuk menerima parameter dan menampilkan help
parser = argparse.ArgumentParser(description='Serviciency - just another simple yet powerful server availability monitoring tools', version='1.0')

parser.add_argument('--date', action="store", help='Date of the logs has been taken')
parser.add_argument('--debug', action="store", help='Show the error of application')

param = parser.parse_args()
date_of_logs = param.date
debug = param.debug

client = MongoClient()
db = client.serviciency
logs = db.logs
results = {}
nodes = logs.distinct("ip_address")
for ip_address in nodes:
    try:
        regx = re.compile("^"+date_of_logs, re.IGNORECASE)
        logs_per_day = logs.find({'ip_address':ip_address, 'date':regx})
        highest_uptime=[]
        for document in logs_per_day:
            highest_uptime.append(document['uptime'])
        highest_uptime = (max(highest_uptime))
        if logs_per_day.count() <= 0:
            print "No records found for ip address %s in %s" % (ip_address, date_of_logs)
        else:
            pipeline = [
                 {
                    "$match": { 'ip_address':ip_address, 'date': regx }
                 },
                 {
                    "$project": { 
                        'hour': { "$substr" : ['$date', 11, 2]},
                        'uptime': '$uptime',
                        'load': '$load', 
                    }
                 },
                 { 
                    "$group": {
                        "_id":"$hour",
                        'uptime': {"$max":"$uptime"},
                        'load': {"$avg":"$load"},
                        'total': {'$sum': 1},
                    } 
                 },
                 {
                    '$sort':{
                        'uptime': -1,
                        'load': -1
                    }
                 }
             ]

            q = logs.aggregate(pipeline)

            results.update({ip_address:[x for x in q]})
    except Exception, e:
        if debug == 'true':
            print "Something wrong must be happen: ", e
        else:
            print "Something wrong must be happen with your parameter or your setting. Please check the help for this command..", e

# mencari loadtime tertinggi dari semua server
print "\n\nTop peak time:"
top_three = []
for result in results:
    temp_dict = find_max_load_time(results[result])
    temp_dict['ip_address'] = result
    top_three.append(temp_dict)
    print temp_dict

# mencari server yang akan dimatikan dengan memilih uptime tertinggi
# kalau ternyata uptimenya sama semua pake load time
if (is_uptime_similar(top_three)):
    killed_server = find_max_up_time(top_three)
else:
    killed_server = find_max_load_time(top_three)
print "\nkilled server: "
print killed_server

# kalau sudah kepilih server mana yang akan dimatikan pilih server yang paling rendah loadtimenya untuk menentukan jam istirahat server
scheduled_hours = find_min_load_time(results[killed_server['ip_address']])
print "\nscheduled time for this server: "
print scheduled_hours