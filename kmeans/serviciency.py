import math
import os
import time
from pymongo import *
from datetime import datetime
import re, pprint, shutil
import argparse
import numpy as np

# menggunakan argument parser untuk menerima parameter dan menampilkan help
parser = argparse.ArgumentParser(description='Serviciency - just another simple yet powerful server availability monitoring tools', version='1.0')

parser.add_argument('--date', action="store", help='Date of the logs has been taken')
parser.add_argument('--debug', action="store", help='Show the error of application')

param = parser.parse_args()
date_of_logs = param.date
debug = param.debug

# variabel global
cluster_attribute = []
data_cluster = []
centroid = []
headers = []
cluster = 3

"""
    fungsi - fungsi yang tidak terlibat clustering k-means
"""
def find_min_load_time(dicto):
    temp_v = 999999999999999
    temp_k = {}

    for k in dicto:
        if k['load'] < temp_v :
            temp_v = k['load']
            temp_k = k

    return temp_k

def find_max_load_time(dicto):
    temp_v = 0.0
    temp_k = {}

    for k in dicto:
        if k['load'] > temp_v :
            temp_v = k['load']
            temp_k = k

    return temp_k 

def find_min_up_time(dicto):
    temp_v = 999999999999999
    temp_k = {}

    for k in dicto:
        if k['uptime'] < temp_v :
            temp_v = k['uptime']
            temp_k = k

    return temp_k

def find_max_up_time(dicto):
    temp_v = 0
    temp_k = {}

    for k in dicto:
        if k['uptime'] > temp_v :
            temp_v = k['uptime']
            temp_k = k

    return temp_k

def is_uptime_similar(dicto):
    is_similar = False
    temp_uptime = dicto[0]['uptime']
    for k in dicto[1:len(dicto)]:
        if k['uptime'] == temp_uptime:
            is_similar = True
        else:
            is_similar = False

    return is_similar
def schedule_down(ip_address, down_hour):
    
    client = MongoClient()
    db = client.serviciency
    node = db.nodes
    list_node = node.find({'ip_address': ip_address})
    
    for data in list_node:
        mac = data['mac']

    
    #fungsi untuk mematikan node server
    schedule_output = open('schedule_down.sh', 'w')
    schedule_output.write('"#!/bin/sh"\n')
    schedule_output.write('cd "/var/www/public/serviciency/"\n')
    schedule_output.write('fab singleHost:ip_address='+ip_address+' shutitdown\n')
    schedule_output.close()
    
    # down hour = jam dengan load terendah
    # up hour = jam untuk menyalakan kembali komputer
    down_hour = int(down_hour)
    up_hour = down_hour + 1

    # ambil tanggal untuk perhitungan serviciency
    # tanggal = time.strftime("%Y-%m-%d");
    tanggal = "2016-06-16";

    # cron_entry = open('/etc/cron.d/cronjob','w')
    # cron_entry.write('MAILTO=""\n')
    # cron_entry.write('*/5 * * * * vagrant /bin/sh /var/www/public/serviciency/getUptime.sh\n')
    # cron_entry.write('59 23 * * * vagrant /usr/bin/python /var/www/public/serviciency/kmeans/serviciency.py --date '+tanggal+'\n')
    # cron_entry.write('* '+str(down_hour)+' * * * vagrant /var/www/public/serviciency/kmeans/schedule_down.sh\n')
    # cron_entry.write('* '+str(up_hour)+' * * * vagrant $(wakeonlan '+mac+')\n')
    # cron_entry.close()

    os.system('crontab -r')
    # os.system('crontab -l | { cat; echo "MAILTO=" ""; } | crontab -')
    os.system('crontab -l | { cat; echo "*/5 * * * * /bin/sh /var/www/public/serviciency/getUptime.sh"; } | crontab -')
    os.system('crontab -l | { cat; echo "59 23 * * * /usr/bin/python /var/www/public/serviciency/kmeans/serviciency.py --date '+tanggal+'"; } | crontab -')
    os.system('crontab -l | { cat; echo "* '+str(down_hour)+' * * * /var/www/public/serviciency/kmeans/schedule_down.sh"; } | crontab -')
    os.system('crontab -l | { cat; echo "* '+str(up_hour)+' * * * wakeonlan '+mac+'"; } | crontab -')

    # os.system("sudo mv /var/www/public/serviciency/kmeans/cronjob /etc/cron.d/")



"""
    fungsi - fungsi yang terlibat dalam perhitungan clustering k-means
"""

# fungsi untuk mencari jarak antar titik dengan menggunakan rumus euclidian distance
def get_euclidean_distance(point_a, point_b):
    temp = 0.0
    for attrib in cluster_attribute:
        temp = temp + (point_a[attrib] - point_b[attrib]) ** 2 
    
    temp2 = math.sqrt( temp ) 
    
    return temp2

# fungsi untuk mencari titik dengan nilai terendah
def find_min_dict(dicto):
        temp_v = 999999999999999
        temp_k = {}

        for k in dicto:
            if k['distance'] < temp_v :
                temp_v = k['distance']
                temp_k = k

        return temp_k

# fungsi untuk melakukan clustering
def do_cluster(centroid, data_cluster):
    i = 0
    for data in data_cluster:
        temp_result = []
        for center in centroid:
            distance = get_euclidean_distance(data, center)
            temp_result.append({ 'distance':distance, 'cluster':center['cluster'] })

        closest_distance = find_min_dict(temp_result)
        
        data_cluster[i]['cluster'] = closest_distance['cluster']
        
        # indeks untuk list
        i = i + 1

    return data_cluster

# fungsi untuk menghasilkan centroid
def generate_centroid(old_centroid, temp_cluster):
    temp_centroid = []
    for centroid in old_centroid:
        num = 0
        summation = {}
        centroid_item = {}

        for attrib in cluster_attribute:
            summation[attrib] = 0.0

        for item in temp_cluster:
            if centroid['cluster'] == item['cluster']:
                for attrib in cluster_attribute:
                    summation[attrib] = summation[attrib] + item[attrib]
                
                num = num + 1
        
        for attrib in cluster_attribute:        
            centroid_item[attrib] = summation[attrib] / num
        
        centroid_item['cluster'] = centroid['cluster']
        # centroid_item['label'] = centroid['label']
        
        temp_centroid.append(centroid_item)

    return temp_centroid

# fungsi untuk memeriksa centroid yang sama
def check_centroid_similarity(old_centroid, new_centroid):
    i = 0
    counter = 0
    for center in old_centroid:
        temp_check = True
        for attrib in cluster_attribute:
            if center[attrib] != new_centroid[i][attrib]: 
                temp_check = True
            else:
                temp_check = False

        if temp_check == True:
            counter = counter + 1
            
        i = i + 1

    return counter

# fungsi untuk mencetak setiap output di cluster
def print_cluster(temp_cluster, format="", temp_centroid=""):
    print "\n====== CLUSTER BARU =====\n"
    if (format=="table"):
        for item in temp_cluster:
            for head in headers:
                print item[head], '\t',
            print item['cluster']

    elif(format == 'cluster'):
        for item in temp_centroid:
            print "\n====> ", item['cluster'].upper(), '\n'
            for data in temp_cluster:
                if (item['cluster'] == data['cluster']):
                    for head in headers:
                        print data[head], '\t',
                    print data['cluster']
            

    else:
        for item in temp_cluster:
            print item

# fungsi untuk memproses data yang dibaca dari file - file yang dihasilkan oleh generate_kmeans()
def exec_kmeans(cluster_attribute, data_cluster, centroid):
    # contoh data yang akan dicluster
    prev_centroid = []
    new_cluster = []

    # main process
    prev_centroid = centroid
    new_cluster = do_cluster(prev_centroid, data_cluster)
    print prev_centroid
    # print_cluster(new_cluster, 'table')

    print "\n====== PENENTUAN CENTROID =====\n"

    while True:
        new_centroid = generate_centroid(prev_centroid, new_cluster)
        print "new centroid: ", new_centroid

        centroid_similarity = check_centroid_similarity(prev_centroid, new_centroid)

        print centroid_similarity

        if (centroid_similarity <= 0):
            break
        elif (centroid_similarity > 0):
            new_cluster = do_cluster(new_centroid, data_cluster)
            prev_centroid = new_centroid
            
    print_cluster(new_cluster, 'cluster', new_centroid)

# fungsi untuk membaca file yang dihasilkan generate_data()
def generate_kmeans(filepath):
    # membaca file yang akan dilakukan pemrosesan clustering
    files = open(filepath, 'r')

    for line in files:
        if line.find('@attribute') == 0:
            attr_list = line.replace('@attribute','').strip()
            for attrib in attr_list.split(','):
                cluster_attribute.append(attrib)
            break

    print cluster_attribute

    files.seek(0)
    for line in files:
        if line.find('@header') == 0:
            header_list = line.replace('@header', '').strip()
            for head in header_list.split(','):
                headers.append(head)
            break

    print headers

    files.seek(0)
    process_data = False
    for line in files:
        if line.find('@data') == 0:
            process_data = True
            continue

        if line.find('@end_data') == 0:
            break

        if process_data:
            temp_cluster_data = line.split(',')
            cluster_data = {}
            
            i = 0
            for head in headers:
                if head in cluster_attribute:
                    cluster_data[head] = float(temp_cluster_data[i].replace('\n', ''))
                else:
                    cluster_data[head] = temp_cluster_data[i].replace('\n', '')
                
                i = i + 1

            cluster_data['cluster'] = ''
            data_cluster.append(cluster_data)

    files.close()

    # menentukan centroid
    print cluster

    for k in range(cluster):
        temp_centroid = data_cluster[k]
        temp_centroid['cluster'] = 'c'+str(k)
        centroid.append(temp_centroid)

    exec_kmeans(cluster_attribute, data_cluster, centroid)

# fngsi untuk menulis file logs sesuai tanggal yang diminta per masing - masing server
def generate_data():
    client = MongoClient()
    db = client.serviciency
    logs = db.logs
    nodes = logs.distinct("ip_address")
    for ip_address in nodes:
        try:
            regx = re.compile("^"+date_of_logs, re.IGNORECASE)
            logs_per_day = logs.find({'ip_address':ip_address, 'date':regx})
            highest_uptime=[]
            for document in logs_per_day:
                highest_uptime.append(document['uptime'])
            highest_uptime = (max(highest_uptime))
            if logs_per_day.count() <= 0:
                print "No records found for ip address %s in %s" % (ip_address, date_of_logs)
            else:
                pipeline = [
                     {
                        "$match": { 'ip_address':ip_address, 'date': regx }
                     },
                     {
                        "$project": { 
                            'hour': { "$substr" : ['$date', 11, 2]},
                            'uptime': '$uptime',
                            'load': '$load', 
                        }
                     },
                     { 
                        "$group": {
                            "_id":"$hour",
                            'uptime': {"$max":"$uptime"},
                            'load': {"$avg":"$load"},
                            'total': {'$sum': 1},
                        } 
                     },
                     {
                        '$sort':{
                            'uptime': -1,
                            'load': -1
                        }
                     }
                 ]

                q = logs.aggregate(pipeline)

                logs_output = open('data/logs_'+ip_address+'_'+date_of_logs+'.txt', 'w')
                logs_output.write('@attribute hour,load,uptime\n')
                logs_output.write('@header hour,load,uptime\n')
                logs_output.write('@data\n')
                for l in q:
                    output = l['_id'] + "," + str(l['load']) + "," + str(l['uptime'])
                    logs_output.write( output + "\n" )
                logs_output.write('@end_data\n')
                logs_output.close()

        except Exception, e:
            if debug == 'true':
                print "Something wrong must be happen: ", e
            else:
                print "Something wrong must be happen with your parameter or your setting. Please check the help for this command..", e

# main script
generate_data()
results = {}
list_of_logs = os.listdir('data')
for filename in list_of_logs:
    print "\n\n\nMemproses file: ", filename, '....'
    print "-----------------------------------------------------------------------"
    
    generate_kmeans('data/'+filename)
    results.update({filename:data_cluster})

    # clear atribut global
    cluster_attribute = []
    headers = []
    data_cluster = []
    centroid = []

# menyiapkan data untuk penentuan jadwal istirahat server, mengelompokkan data setiap hasil clustering pada masing - masing node
results2 = {}
for node in results:
    temp_result = results[node]
    clusters = {}
    for i in range(0, cluster):
        clusters.update({'c'+str(i):[]})

    for data in temp_result:
        for i in range(0, cluster):
            if data['cluster'] == 'c'+str(i):
                clusters[data['cluster']].append(data)        
            else:
                continue
    results2.update({node:clusters})

print "\n===========> PROSES CLUSTERING K-MEANS SELESAI....\n"

# mencari max load time di tiap cluster di tiap node
print "\npeak load time dan uptime dari semua nodes:"
top_three = []
for node in results2:
    top_three_in_node = []
    for i in range(0, cluster):
        cluster_name = 'c'+str(i)
        temp_dict = find_max_load_time (results2[node][cluster_name]) 
        temp_dict['name'] = node
        top_three_in_node.append( temp_dict )

    top_three.append(find_max_load_time(top_three_in_node))

# mencari max up time di top three
print top_three
temp_dict = find_max_up_time(top_three)
print "\nserver yang akan dimatikan adalah:"
print temp_dict
final_node = temp_dict['name'].split('_')
final_node = final_node[1]


# menentukan jadwal pematian server
print "\njadwal untuk mematikan server adalah:"
bottom_three = []
for i in range(0, cluster):
    cluster_name = 'c'+str(i)
    sched_dict = find_min_load_time(results2[temp_dict['name']][cluster_name])
    bottom_three.append( sched_dict )

min_time = find_min_load_time( bottom_three )
print min_time
schedule_down(final_node, min_time['hour'])
shutil.rmtree('data')
os.mkdir('data')