from pymongo import *
from datetime import datetime
import re
import argparse
import numpy as np

# menggunakan argument parser untuk menerima parameter dan menampilkan help
parser = argparse.ArgumentParser(description='Serviciency - just another simple yet powerful server availability monitoring tools', version='1.0')

parser.add_argument('--date', action="store", help='Date of the logs has been taken')
parser.add_argument('--debug', action="store", help='Show the error of application')

param = parser.parse_args()
date_of_logs = param.date
debug = param.debug

client = MongoClient()
db = client.serviciency
logs = db.logs
nodes = logs.distinct("ip_address")
for ip_address in nodes:
	try:
		regx = re.compile("^"+date_of_logs, re.IGNORECASE)
		logs_per_day = logs.find({'ip_address':ip_address, 'date':regx})
		highest_uptime=[]
		for document in logs_per_day:
			highest_uptime.append(document['uptime'])
		highest_uptime = (max(highest_uptime))
		if logs_per_day.count() <= 0:
			print "No records found for ip address %s in %s" % (ip_address, date_of_logs)
		else:
			pipeline = [
			     {
			     	"$match": { 'ip_address':ip_address, 'date': regx }
			     },
			     {
			     	"$project": { 
			     		'hour': { "$substr" : ['$date', 11, 2]},
			     		'uptime': '$uptime',
			     		'load': '$load', 
			     	}
			     },
			     { 
			     	"$group": {
			     		"_id":"$hour",
			     		'uptime': {"$max":"$uptime"},
			     		'load': {"$avg":"$load"},
			     		'total': {'$sum': 1},
			     	} 
			     },
			     {
			     	'$sort':{
			     		'uptime': -1,
			     		'load': -1
			     	}
			     }
			 ]

			q = logs.aggregate(pipeline)

			logs_output = open('data/logs_'+ip_address+'_'+date_of_logs+'.txt', 'w')
			logs_output.write('@attribute hour,load,uptime\n')
			logs_output.write('@header hour,load,uptime\n')
			logs_output.write('@data\n')
			for l in q:
				output = l['_id'] + "," + str(l['load']) + "," + str(l['uptime'])
				logs_output.write( output + "\n" )
			logs_output.write('@end_data\n')
			logs_output.close()

	except Exception, e:
		if debug == 'true':
			print "Something wrong must be happen: ", e
		else:
			print "Something wrong must be happen with your parameter or your setting. Please check the help for this command..", e

