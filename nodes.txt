db.nodes.insert({
	ip_address: "192.168.33.11", mac: "08:00:27:66:0b:b2", status: "up"
})
db.nodes.insert({
	ip_address: "192.168.33.12", mac: "08:00:27:28:14:5e", status: "up"
})
db.nodes.insert({
	ip_address: "192.168.33.13", mac: "08:00:27:0b:12:b8", status: "up"
})
db.test.insert([
	{ nama: "yogi", nilai: "0.4" },
	{ nama: "asep", nilai: "0.4" },
	{ nama: "nono", nilai: "0.4" },
	{ nama: "joni", nilai: "0.4" },
	{ nama: "gani", nilai: "0.4" },
	{ nama: "nana", nilai: "0.4" }
])

db.logs.find().forEach(function(x){
	x.load=parseFloat(x.load);
	db.logs.update(
		{'_id':x._id},
		{$set:{'load':x.load}}
	);
});

db.mycol.update(
	{'title':'MongoDB Overview'},
	{$set:{'title':'New MongoDB Tutorial'}}
)